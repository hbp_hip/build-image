FROM ubuntu:18.04

RUN apt-get update && apt-get install -y wget gnupg2 \
  && wget -O- http://neuro.debian.net/lists/bionic.de-m.full | tee /etc/apt/sources.list.d/neurodebian.sources.list \
  && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0xA5D32F012649A5A9 \
  && apt-get update >> apt.log \
  && apt-get install -y ruby ruby-dev rubygems build-essential gdebi-core wget curl ca-certificates \
      ninja-build cmake-curses-gui gcc g++ zlib1g-dev git gnupg >> apt.log \
  && gem install --no-document fpm
